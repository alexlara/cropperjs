# coding=utf-8

import sys
import re
from base64 import b64decode

from django.core.files.base import ContentFile
from django.utils.text import slugify

url_pattern = "^https?:\\/\\/(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$"

# Used for consistent string/type comparisons across Python 2 and 3
# without requiring six, future or other external dependencies
TEXT_TYPE = str if sys.version_info[0] >= 3 else unicode


def check_url(data):
    return re.match(url_pattern, data)

def cropperImageFile(base64data, defaultName):
    try:
        metadata, imagedata = base64data.split("base64")
        filename, mimetype = metadata[:-1].split(";")
    except:
        # Too many values to unpack, unexpected format
        raise ValueError
    if 'filename' in metadata:
        filename = metadata.split(";")[0].split(":")[1]
        filename = slugify(filename)
    else:
        filename = defaultName
    fileext = mimetype.split("/")[1]

    return ContentFile(b64decode(imagedata), "%s.%s" % (filename, fileext))

